from Packet import *
import socket
import os
import signal
import sys
import helpers
import timeit

start = timeit.default_timer()

os.system('clear')

# Constants
__HOST      = sys.argv[1]
__PORT      = int(sys.argv[2])
WINDOW_SIZE = 5
OUT_FILE    = 'output.txt'
INP_FILE = str(sys.argv[3]) if len(sys.argv[3]) >= 3 else 'input.txt'
CHUNK_SIZE  = 100 # 512 bytes at packet

server_address = (__HOST, __PORT)
connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
connection.bind(('localhost', 0))

helpers.connection = connection

print 'Connecting...'

output_file = open(OUT_FILE, 'a')

signal.signal(signal.SIGINT, helpers.close_server)

# Globals
src_port  = connection.getsockname()[1]
packets   = {}
seq_num   = 0 	# Current sequance number
pkts_sent = 0	# Number of packets have been sent

try:
	# Send SYN packet (handshake)
	handshake_pkt = helpers.create(src_port, __PORT, seq_num, helpers.set_flags(SYN=True, ACK=False), INP_FILE)
	connection.sendto(handshake_pkt, server_address)

	# Begin to receive packets
	while True:
		msg, address = connection.recvfrom(helpers.HEADER_LENGTH + CHUNK_SIZE)

		header, data = helpers.unpack(msg, False)
		header_dict = helpers.dictionarify(header)

		# print 'i received %s' % data

		# If packet is not valid
		if not helpers.isValid(msg):
			# just ignore it
			# print 'corrupted !'
			helpers.details(header, data, 'CORR')
			continue
		else:
			helpers.details(header, data, 'RECV')

		# Sender got our SYN and ACKing it
		if header_dict['SYN'] and header_dict['ACK']:
			continue
		
		# If last packet Area
		if header_dict['FIN']:
			if header_dict['ACK']:
				continue
			else:
				output_file.close()
				# header_dict['seq_num'] += 1
				flags = helpers.set_flags(FIN=True, ACK=True)
				connection.settimeout(3)
		else:

			# Check if duplicate (acked) packet came from sender
			if header_dict['seq_num'] in packets :
				# Send ACK again
				packets[ header_dict['seq_num'] ].send(connection, server_address, 'RACK')
				continue


			if data == 'EOP':
				# END OF PACKETS
				# print 'i am sending EOP'
				flags = helpers.set_flags(FIN=True, ACK=False)
				header_dict['seq_num'] += 1
			else:
				flags = helpers.set_flags()
				# Write to output file
				output_file.write(data)
				# add ACKed packet to hashtable
				packets[ header_dict['seq_num'] ] = Packet(msg)


		# Send ACK to server
		ack_packet = helpers.create(src_port, __PORT, header_dict['seq_num'], flags)
		connection.sendto(ack_packet, server_address)






	

except socket.timeout:
	print 'Closing...'
	connection.close()

connection.close()



stop = timeit.default_timer()

print 'Elapsed Time: ', stop - start 