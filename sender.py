from Packet import *
import socket
import os
import signal
import sys
import helpers
import math
from random import *
import time

# random.seed(time.time())

os.system('clear')

# Constants
__HOST      = sys.argv[1]
__PORT      = int(sys.argv[2])
WINDOW_SIZE = 5
INP_FILE	= 'input.txt'
CHUNK_SIZE	= 100 # 2 bytes at packet

server_address = (__HOST, __PORT)
connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
connection.bind(server_address)

helpers.connection = connection

print 'Connecting...'

# input_file = open(INP_FILE, 'r')

signal.signal(signal.SIGINT, helpers.close_server)


file_size = os.path.getsize(INP_FILE)
packets_should_be_sent = math.ceil(float(file_size) / float(CHUNK_SIZE))   #number of packets
# lossProbability = randint(0, 10) / 10.0
lossProbability = randint(0, 10) / 10.0
packets_should_be_lost = int(packets_should_be_sent * lossProbability)
print 'LOSS PROBABILITY: %f, packets_should_be_lost %d, packets_should_be_sent %d' % (lossProbability, packets_should_be_lost, packets_should_be_sent)

def should_i_send():
	global packets_should_be_sent
	global packets_should_be_lost

	if packets_should_be_sent:
		toSentOrNotToSent = randint(0,1)
		if(packets_should_be_lost == 0):
			#send all directly
			packets_should_be_sent -= 1
			return True
	 	elif(packets_should_be_sent == packets_should_be_lost):
	 		#all remaining packets will be lost and retransmitted
	 		packets_should_be_sent -= 1 
	 		packets_should_be_lost -= 1
	 		return False
	 	else:
			if(toSentOrNotToSent == 1):
				#send
				packets_should_be_sent-=1
				return True

			else:
				#packet is lost :O
				packets_should_be_lost -= 1
				return False


# Globals area
packets   = {} 	# packets hashtable to hold packets in window
seq_num   = 0 	# Current sequance number
pkts_sent = 0	# Number of packets have been sent
SOW		  = 1 	# Start Of Window
window 	  = {}
ended 	  = False
last_pkt_num = 0

try:
	while True:
		msg, address = connection.recvfrom(1024)
		client_address = address

		client_address = address

		header, data = helpers.unpack(msg, False)
		header_dict  = helpers.dictionarify(header);

		# If package is not valid ignore
		# until timeout ticks
		if not helpers.isValid(msg):
			continue
		else:
			helpers.details(header, data, 'RECV')

		# First Handshake
		if header_dict['SYN']:
			# respond with ACK
			INP_FILE = data
			# print 'input', INP_FILE
			input_file = open(INP_FILE, 'r')

			ack_packet = helpers.create(__PORT, header_dict['src_port'], seq_num, helpers.set_flags(SYN=True))
			connection.sendto(ack_packet, client_address)

		# Final ack
		if header_dict['FIN'] and not header_dict['ACK'] :
			# Receiver want to FIN
			# respond with ACK
			seq_num = header_dict['seq_num']

			ack_pkt = helpers.create(__PORT, header_dict['src_port'], seq_num, helpers.set_flags(FIN=True, ACK=True))
			connection.sendto(ack_pkt, client_address)

			seq_num += 1

			# Send FIN packet
			fin_pkt = helpers.create(__PORT, header_dict['src_port'], seq_num, helpers.set_flags(FIN=True, ACK=False))
			connection.sendto(fin_pkt, client_address)

			# ACK from receiver
			msg, address = connection.recvfrom(1024)
			client_address = address

			header, data = helpers.unpack(msg)
			header_dict = helpers.dictionarify(header);

			sorted_packets = sorted(packets.keys())

			for x in range(-1, -1 * WINDOW_SIZE -1, -1):
				key = sorted_packets[x]
				if key in packets:
					packets[ key ].mark_as_acked()

			

			break


		# Receive ACK Area
		if header_dict['ACK'] and header_dict['seq_num'] in packets:
			# If duplicate ACK
			if  packets[ header_dict['seq_num'] ].acked :
				continue # just ignore it
			else:
				# update seq number
				seq = header_dict['seq_num']
				# print 'i will ackwed %d' % seq

				packets[ seq ].mark_as_acked()
				if SOW == seq:
					# REMOVE CURRENT PACKET FROM WINDOW
					window.pop(seq, None)
					# This loop will get the last sequance
					# of ACKed packet
					while seq in packets and packets[seq].acked :
						seq +=  1

					# Update SOW with first unAcked packet
					SOW = seq


		if ended:
			continue
		
		# print 'enter loop', ended
		while len(window) < WINDOW_SIZE and not ended:
			# Send new Packets
			seq_num += 1

			# Go to the point we want in file
			input_file.seek(seq_num * CHUNK_SIZE - CHUNK_SIZE, 0)
			data = input_file.read(CHUNK_SIZE)

			if data == '':
				# Final Packet
				flags = helpers.set_flags(ACK=False)
				data = 'EOP'
				ended = True

				# pkt = helpers.create(__PORT, header_dict['src_port'], seq_num, flags, data)
				# connection.sendto(pkt, client_address)

				# continue
			else:
				flags = helpers.set_flags(ACK=False)

			new_pkt = Packet(helpers.create(__PORT, header_dict['src_port'], seq_num, flags, data, False))
			
			if data == 'EOP' or (seq_num == 0):
				new_pkt.send(connection, client_address)
			elif should_i_send():
				new_pkt.send(connection, client_address)
			else:
				helpers.details(new_pkt.header, new_pkt.data, 'ZIZO')

			new_pkt.start_timer(connection, client_address)

			packets[ seq_num ] = new_pkt
			last_pkt_num = seq_num
			window[ seq_num] = new_pkt

			# helpers.details(new_pkt.header, new_pkt.data, 'SEND')

		# print 'exit loop', ended

except socket.timeout:
	print 'Closing...'
	connection.close()

print 'Closing...'
connection.close()